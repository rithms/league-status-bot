__author__ = 'rithms'

import configparser
import praw
import time
import requests

USER_AGENT = '/u/LeagueStatus bot by /u/rithms'
SIGNATURE = '''
------
[^LeagueStatus](https://github.com/rithms/LeagueStatusBot) \
^by [^Rithms](https://github.com/rithms)
------
'''
SLEEP = 2

reddit = praw.Reddit(user_agent=USER_AGENT)

#Login
config = configparser.ConfigParser()
config.read('settings.config')
username = config.get('login', 'username')
password = config.get('login', 'password')
print('Logging in...\n')
reddit.login(username, password)
print('Login successful...\n')

triggers = ['!leaguestatusbot', '!leaguestatus']
regions = ['na', 'br', 'eune', 'euw', 'lan', 'las', 'oce', 'pbe', 'ru', 'tr']
flagged = set()
num_requests = 0
num_replies = 0

def run():
    global num_requests
    global num_replies
    subreddit = reddit.get_subreddit('leagueoflegends')
    comments = subreddit.get_comments()
    print('Searching comments in subreddit... \n')
    for comment in comments:
        comment_body = comment.body.lower().split()
        try:
            if comment_body[0] in triggers:
                if comment.id not in flagged:
                    print('Request found!\n')
                    flagged.add(comment.id)
                    num_requests += 1
                    _region = 'na'
                    try:
                        for region in regions:
                            if comment_body[1] == region:
                                _region = comment_body[1]
                    except:
                        pass

                    print('Replying...\n')
                    reply = get_reply(_region)
                    comment.reply(reply)
                    num_replies += 1
                    print('Reply successful!\n')
                    print('Taking a break...!\n')
                    time.sleep(600)

        except:
            pass
    print("Number of Requests: " + str(num_requests))
    print("Number of Replies: " + str(num_replies))

def get_reply(region = 'na'):
    print("Fetching service status...")
    if region == 'pbe':
        url = 'http://status.pbe.leagueoflegends.com/shards/' + region
    else:
        url = 'http://status.leagueoflegends.com/shards/' + region
    r = requests.get(url)
    if r.status_code == 429:
        print("Rate limit exceeded...Sleeping...")
        time.sleep(10)
        return get_reply(region)
    elif r.status_code == 200:
        json = r.json()
        reply = '**Service Status for ' + json['name'] + ':** \n'
        for service in json['services']:
            reply += "> **" + service['name'] + ":** " + service['status'].title() + '\n\n'
            if service['incidents']:
                incidents = service['incidents']
                for incident in incidents:
                    for update in incident['updates']:
                        reply += '> * ' + update['severity'].title() + ": " + update['content'] + ' \n\n'

        return reply + SIGNATURE


while True:
    run()
    time.sleep(SLEEP)





