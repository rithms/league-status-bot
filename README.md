# LeagueStatusBot
A League of Legends Reddit bot that posts the current Service Status upon request on the /r/leagueoflegends subreddit.

# Disclaimer
LeagueStatusBot isn't endorsed by Riot Games and doesn't reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.

# Making a request
```
!LeagueStatus <Server>
!LeagueStatusBot <Server>
```

# Supported Servers

**[BR, EUNE, EUW, LAN, LAS, NA, OCE, PBE, RU, TR]**

# Example

![example](http://i.imgur.com/hU6p486.png)

# settings.cfg
To use this bot yourself, you will need to create a config file with the following format, using your own login credentials.
```
[login]
username=**************
password=**************
